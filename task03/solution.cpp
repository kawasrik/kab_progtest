#ifndef __PROGTEST__
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <unistd.h>
#include <string>
#include <memory>
#include <vector>
#include <fstream>
#include <cassert>
#include <cstring>

#include <openssl/evp.h>
#include <openssl/rand.h>

using namespace std;

struct crypto_config
{
	const char * m_crypto_function;
	std::unique_ptr<uint8_t[]> m_key;
	std::unique_ptr<uint8_t[]> m_IV;
	size_t m_key_len;
	size_t m_IV_len;
};

#endif /* _PROGTEST_ */
/**
 * @file solution.cpp
 * @author Riku Kawasaki (kawasrik@fit.cvut.cz)
 * @brief HW03
 * @date 2024-04-07
 * 
 * @copyright Copyright (c) 2024
 * 
 */

/**
 * Observation (comparison between ECB and CBC):
 * Since the same blocks of PT always have the same outcome in ECB, 
 * it is easier to guess the PT from the CT encrypted by ECB than CBC.
 * You can tell the rough silhouette of the character in the _enc_cbc.TGA file.
 * 
*/

struct EVP_CIPHER_CTX_Deleter {
	void operator()(EVP_CIPHER_CTX* ctx) const {
		EVP_CIPHER_CTX_free(ctx);
	}
};
using EVP_CIPHER_CTX_UPtr = unique_ptr<EVP_CIPHER_CTX, EVP_CIPHER_CTX_Deleter>;

bool configIsValid(const EVP_CIPHER* cipher, crypto_config& config, bool encrypt ) 
{
	int keyLenReq = EVP_CIPHER_key_length(cipher);	// The key length required
	if ((int)config.m_key_len < keyLenReq) {
		if  ( !encrypt )
			return false;
		config.m_key_len = keyLenReq;
		config.m_key = nullptr;
	}
	if ( !config.m_key  ) 
	{
		if  ( !encrypt ) {
			return false;
		}
		// Generate a sufficient new key.
		uint8_t* key = new uint8_t[config.m_key_len];
		RAND_bytes(key, config.m_key_len);
		config.m_key = unique_ptr<uint8_t[]>(key);
	}

	int ivLenReq = EVP_CIPHER_iv_length(cipher);
	if ( ivLenReq > 0 ) {
		if (  (int)config.m_IV_len < ivLenReq ) {
			if ( !encrypt ) return false;
			config.m_IV_len = ivLenReq;
			config.m_IV = nullptr;
		}
		if ( ! config.m_IV  ) 
		{
			if ( !encrypt ) {
				return false;
			}
			uint8_t* iv = new uint8_t[config.m_IV_len];
			RAND_bytes(iv, config.m_IV_len);
			config.m_IV = unique_ptr<uint8_t[]>(iv);
		}
	}

	return true;
}

bool cryptOperation(const std::string & in_filename, const std::string & out_filename, 
	crypto_config & config, bool encrypt)
{
	EVP_CIPHER_CTX_UPtr ctxPtr(EVP_CIPHER_CTX_new());
	if ( ! ctxPtr ) return false;
	const EVP_CIPHER* cipher = EVP_get_cipherbyname(config.m_crypto_function);
	if ( !cipher || !configIsValid(cipher, config, encrypt) ) 
	{
		return false;
	}
	if ( !EVP_CipherInit_ex(ctxPtr.get(), cipher, NULL, config.m_key.get(), config.m_IV.get(), encrypt ? 1 : 0 ) )
	{
		return false;
	}

	ifstream inFile (in_filename, std::ios::binary);
	ofstream outFile ( out_filename,  std::ios::binary );
	if (!inFile.is_open() || ! outFile.is_open() ) {
		return false;
	}
	// Copy the first 18 bytes.
	for ( int i = 0; i < 18; i ++ ) {
		char byte;
		if ( ! inFile.good() || ! inFile.read(&byte, 1) ) {
			return false;
		}
		outFile.write(&byte, 1);
		if ( !outFile.good() ) {
			return false;
		}
	}

	int outLen;
	vector<uint8_t> inBuff(1);
	vector<uint8_t> outBuff(1+ EVP_CIPHER_CTX_block_size(ctxPtr.get()) );
	while ( inFile.good() ) 
	{
		// printf("here in the loop\n");
		inFile.read((char*) inBuff.data(), inBuff.size());
		int bytesRead = (int) inFile.gcount();
		if ( ! EVP_CipherUpdate(ctxPtr.get(), outBuff.data(), &outLen, inBuff.data(), bytesRead) )
		{
			return false;
		}
		outFile.write((const char*) outBuff.data(), outLen);
		if ( !outFile.good() ) {
			return false;
		}
	}

	if ( !EVP_CipherFinal_ex(ctxPtr.get(), outBuff.data(), &outLen ) ) {
		return false;
	}
	outFile.write((const char*) outBuff.data(), outLen);
	if ( !outFile.good() ) {
		return false;
	}
	
	return true;
}

bool encrypt_data ( const std::string & in_filename, const std::string & out_filename, crypto_config & config )
{
	return cryptOperation(in_filename, out_filename, config, true);
}

bool decrypt_data ( const std::string & in_filename, const std::string & out_filename, crypto_config & config )
{
	return cryptOperation(in_filename, out_filename, config, false);
}


#ifndef __PROGTEST__

bool compare_files ( const char * name1, const char * name2 )
{
	ifstream file1 (name1, std::ios::binary );
	ifstream file2 (name2, std::ios::binary );
	if ( !file1 || !file2 ) {
		return false;
	}
	size_t cnt = 0;
	char byte1, byte2;
	// while ( file1.read(&byte1, sizeof(char)) && file2.read(&byte2, sizeof(char) ) )
	while ( file1.good() && file2.good() )
	{
		file1.read(&byte1, 1);
		file2.read(&byte2, 1);
		if ( byte1 != byte2 ) {
			printf("here\n");
			printf("cnt: %zu\n", cnt);
			return false;
		}
		cnt ++;
	}
	printf("cnt: %zu\n", cnt);
	return true;
}

int main ( void )
{
	crypto_config config {nullptr, nullptr, nullptr, 0, 0};

	// ECB mode
	config.m_crypto_function = "AES-128-ECB";
	config.m_key = std::make_unique<uint8_t[]>(16);
 	memset(config.m_key.get(), 0, 16);
	config.m_key_len = 16;

	assert( encrypt_data  ("homer-simpson.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "homer-simpson_enc_ecb.TGA") );

	assert( decrypt_data  ("homer-simpson_enc_ecb.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "homer-simpson.TGA") );

	assert( encrypt_data  ("UCM8.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "UCM8_enc_ecb.TGA") );

	assert( decrypt_data  ("UCM8_enc_ecb.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "UCM8.TGA") );

	assert( encrypt_data  ("image_1.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "ref_1_enc_ecb.TGA") );

	assert( encrypt_data  ("image_2.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "ref_2_enc_ecb.TGA") );

	assert( decrypt_data ("image_3_enc_ecb.TGA", "out_file.TGA", config)  &&
		    compare_files("out_file.TGA", "ref_3_dec_ecb.TGA") );

	assert( decrypt_data ("image_4_enc_ecb.TGA", "out_file.TGA", config)  &&
		    compare_files("out_file.TGA", "ref_4_dec_ecb.TGA") );

	// CBC mode
	config.m_crypto_function = "AES-128-CBC";
	config.m_IV = std::make_unique<uint8_t[]>(16);
	config.m_IV_len = 16;
	memset(config.m_IV.get(), 0, 16);

	assert( encrypt_data  ("UCM8.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "UCM8_enc_cbc.TGA") );

	assert( decrypt_data  ("UCM8_enc_cbc.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "UCM8.TGA") );

	assert( encrypt_data  ("homer-simpson.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "homer-simpson_enc_cbc.TGA") );

	assert( decrypt_data  ("homer-simpson_enc_cbc.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "homer-simpson.TGA") );

	assert( encrypt_data  ("image_1.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "ref_5_enc_cbc.TGA") );

	assert( encrypt_data  ("image_2.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "ref_6_enc_cbc.TGA") );

	assert( decrypt_data ("image_7_enc_cbc.TGA", "out_file.TGA", config)  &&
		    compare_files("out_file.TGA", "ref_7_dec_cbc.TGA") );

	assert( decrypt_data ("image_8_enc_cbc.TGA", "out_file.TGA", config)  &&
		    compare_files("out_file.TGA", "ref_8_dec_cbc.TGA") );
	
	// Additional tests -------------------------------------------------------------------------
	assert( encrypt_data ("advice/in_5682631.bin", "out_file.TGA", config)  &&
		    compare_files("out_file.TGA", "advice/ref_5682631.bin") );

	return 0;
}

#endif /* _PROGTEST_ */
