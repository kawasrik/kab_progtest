#ifndef __PROGTEST__
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <string_view>
#include <memory>
#include <vector>
#include <fstream>
#include <cassert>
#include <cstring>

#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/pem.h>

using namespace std;

#endif /* __PROGTEST__ */

struct EVP_CIPHER_CTX_Deleter {
	void operator()(EVP_CIPHER_CTX* ctx) const {
		EVP_CIPHER_CTX_free(ctx);
	}
};
using EVP_CIPHER_CTX_UPtr = unique_ptr<EVP_CIPHER_CTX, EVP_CIPHER_CTX_Deleter>;

struct EVP_PKEY_Deleter {
    void operator()(EVP_PKEY* key) const {
        EVP_PKEY_free(key);
    }
};
using EVP_PKEY_UPtr = unique_ptr<EVP_PKEY, EVP_PKEY_Deleter>;

struct FILE_Deleter {
    void operator()(FILE* file) const {
        fclose(file);
    }
};
using FILE_UPtr = unique_ptr<FILE, FILE_Deleter>;

// struct Text_Deleter {
//     void operator()(unsigned char* text) const {
//         delete[] text;
//     }
// };
// using Text_UPtr = unique_ptr<unsigned char, Text_Deleter>;

bool seal( string_view inFile, string_view outFile, string_view publicKeyFile, string_view symmetricCipher )
{
    //waiting for code...
    // If any of the file name is empty, return falses
    if ( inFile.empty() || outFile.empty() || publicKeyFile.empty() || symmetricCipher.empty() ) {
        return false;
    }

    ifstream ifs(inFile.data(), ios::binary);
    ofstream ofs(outFile.data(), ios::binary);
    FILE_UPtr pbkPtr(fopen(publicKeyFile.data(), "r")); // public key file pointer
    // If any of the file is not open, return false
    if ( !ifs || !pbkPtr || !ofs ) {
        remove(outFile.data());
        return false;
    }

    EVP_PKEY_UPtr pkeyPtr (PEM_read_PUBKEY(pbkPtr.get(), nullptr, nullptr, nullptr));
    EVP_CIPHER_CTX_UPtr ctx(EVP_CIPHER_CTX_new());
    const EVP_CIPHER* cipher = EVP_get_cipherbyname(symmetricCipher.data());

    if ( !pkeyPtr || !ctx || !cipher ) {
        remove(outFile.data());
        return false;
    }

    int nid = EVP_CIPHER_nid(cipher);
    // cout << "nid(seal): " << nid << endl;
    vector<unsigned char> ek(EVP_PKEY_size(pkeyPtr.get()));
    const int ivLen = EVP_CIPHER_iv_length(cipher);
    // cout << "ivLen(seal): " << ivLen << endl;
    vector<unsigned char> iv;
    unsigned char* ivData = nullptr;
    if ( ivLen > 0 ) {
        iv = vector<unsigned char>(ivLen);
        ivData = iv.data();
    } else if ( ivLen < 0 ) {
        remove(outFile.data());
        return false;
    }

    int ekLen = 0;
    EVP_PKEY* pkLval = pkeyPtr.get();   // lvalue pointer
    unsigned char* ekLval = ek.data();   // lvalue pointer
    if ( EVP_SealInit(ctx.get(), cipher, &ekLval, &ekLen, ivData, &pkLval, 1) != 1
        || !ofs.write((char*)&nid, sizeof(int)) 
        || !ofs.write((char*)&ekLen, sizeof(int)) 
        || !ofs.write((char*)ek.data(), ekLen) ) 
    {
        remove(outFile.data());
        return false;
    }
    // if IV is necessary, write IV to the file
    if ( ivLen > 0 ) {
        if ( !ofs.write((char*)ivData, ivLen) ) {
            remove(outFile.data());
            return false;
        }
    }

    // Encrypt the file
    int inBuffLen = 1024;
    // int inBuffLen = EVP_CIPHER_CTX_block_size(ctx.get());
    vector<unsigned char> inBuff(inBuffLen);
    vector<unsigned char> outBuff(inBuffLen + EVP_CIPHER_CTX_block_size(ctx.get()));
    int outLen = 0;
    while (ifs.good()) {
        ifs.read((char*)inBuff.data(), inBuffLen);
        if (EVP_SealUpdate(ctx.get(), outBuff.data(), &outLen, inBuff.data(), ifs.gcount()) != 1)
        {
            remove(outFile.data());
            return false;
        }
        ofs.write((char*)outBuff.data(), outLen);
        if ( ! ofs.good() ) {
            remove(outFile.data());
            return false;
        }
    }
    if ( EVP_SealFinal(ctx.get(), outBuff.data(), &outLen) != 1 )
    {
        remove(outFile.data());
        return false;
    }
    ofs.write((char*)outBuff.data(), outLen);
    if ( ! ofs.good() ) {
        remove(outFile.data());
        return false;
    }
    
    return true;
}


bool open( string_view inFile, string_view outFile, string_view privateKeyFile )
{
    //waiting for code...
    // If any of the file name is empty, return false
    if ( inFile.empty() || outFile.empty() || privateKeyFile.empty() ) {
        remove(outFile.data());
        return false;
    }

    ifstream ifs(inFile.data(), ios::binary);
    ofstream ofs(outFile.data(), ios::binary);
    FILE_UPtr pkPtr(fopen(privateKeyFile.data(), "r"));
    // If any of the file is not open, return false
    if ( !ifs || !pkPtr || !ofs ) {
        remove(outFile.data());
        return false;
    }
    
    EVP_PKEY_UPtr pkeyPtr (PEM_read_PrivateKey(pkPtr.get(), nullptr, nullptr, nullptr));
    EVP_CIPHER_CTX_UPtr ctx(EVP_CIPHER_CTX_new());
    if ( ! pkeyPtr || !ctx ) {
        remove(outFile.data());
        return false;
    }

    // Read the cipher type and the encrypted key length
    int nid = 0;
    if ( !ifs.read((char*)&nid, sizeof(int)) ) {
        remove(outFile.data());
        return false;
    }
    // cout << "nid(open): " << nid << endl;
    // cout << "cipher name: " << EVP_CIPHER_name(EVP_get_cipherbynid(nid)) << endl;
    const EVP_CIPHER* cipher = EVP_get_cipherbynid(nid);
    if ( !cipher ) {
        remove(outFile.data());
        return false;
    }

    int ekLen = 0;
    if ( !ifs.read((char*)&ekLen, sizeof(int)) 
        || ekLen <= 0 
        || ekLen > EVP_PKEY_size(pkeyPtr.get()) )
    {
        remove(outFile.data());
        return false;
    }
    // cout << "ekLen(open): " << ekLen << endl;
    vector<unsigned char> ek(ekLen);
    if ( !ifs.read((char*)ek.data(), ekLen) ) {
        remove(outFile.data());
        return false;
    }
    vector<unsigned char> iv;
    unsigned char* ivData = nullptr;
    const int ivLen = EVP_CIPHER_iv_length(cipher);
    // cout << "ivLen(open): " << ivLen << endl;
    if ( ivLen > 0 ) {
        iv = vector<unsigned char>(ivLen);
        if ( !ifs.read((char*)iv.data(), ivLen) ) {
            remove(outFile.data());
            return false;
        }
        ivData = iv.data();
    } else if ( ivLen < 0 ) {
        remove(outFile.data());
        return false;
    }
    if (EVP_OpenInit(ctx.get(), cipher, ek.data(), ekLen, ivData, pkeyPtr.get()) != 1) {
        remove(outFile.data());
        return false;
    }

    const int inBuffLen = 1024;
    // int inBuffLen = EVP_CIPHER_CTX_block_size(ctx.get());
    vector<unsigned char> inBuff(inBuffLen);
    vector<unsigned char> outBuff(inBuffLen + EVP_CIPHER_CTX_block_size(ctx.get()));
    int outLen;
    while (ifs.good()) {
        ifs.read((char*)inBuff.data(), inBuffLen);
        if (EVP_OpenUpdate(ctx.get(), outBuff.data(), &outLen, inBuff.data(), ifs.gcount()) != 1) {
            remove(outFile.data());
            return false;
        }
        // cout << "outLen: " << outLen << endl;
        ofs.write((char*)outBuff.data(), outLen);
        if ( ! ofs.good() ) {
            remove(outFile.data());
            return false;
        }
    }
    if ( EVP_OpenFinal(ctx.get(), outBuff.data(), &outLen) != 1 ) {
        remove(outFile.data());
        return false;
    }
    ofs.write((char*)outBuff.data(), outLen);
    if ( !ofs.good() ) {
        remove(outFile.data());
        return false;
    }

    return true;
}



#ifndef __PROGTEST__

int main ( void )
{
    assert( seal("fileToEncrypt", "sealed.bin", "PublicKey.pem", "aes-128-cbc") );
    assert( open("sealed.bin", "openedFileToEncrypt", "PrivateKey.pem") );

    assert( open("sealed_sample.bin", "opened_sample.txt", "PrivateKey.pem") );

    // assert( seal("fileToEncrypt", "sealed.bin", "PublicKey.pem", "aes-128-ecb") );
    // assert( open("encrypted.bin", "opened_encrypted.txt", "PrivateKey.pem") );

    return 0;
}

#endif /* __PROGTEST__ */

