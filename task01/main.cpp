#ifndef __PROGTEST__
#include <assert.h>
#include <stdio.h>

#include <iostream>

#endif /* __PROGTEST__ */

int addNumbers(int a, int b) {
    //TODO: Your code here
    return a+b;
}

#ifndef __PROGTEST__

int main (void) {
    assert(addNumbers(0, 1) == 1);
    assert(addNumbers(1, 0) == 1);
    assert(addNumbers(10, 20) == 30);
    assert(addNumbers(20, 10) == 30);
    assert(addNumbers(-5, 5) == 0);
    return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */

