#ifndef __PROGTEST__
#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

#include <openssl/evp.h>
#include <openssl/rand.h>

using namespace std;

#endif /* __PROGTEST__ */

int cntZerosInStrRaw(const string& strRaw) {
    int cnt = 0;
    for ( unsigned char ch : strRaw ) {
        if ( ch == 0 ) {
            cnt += 8;
        } else {
            while ((ch & 0x80) == 0 ) {
                cnt ++;
                ch <<= 1;
            }
            break;
        }
    }
    return cnt;
}
bool computeHash(string &outputStrRaw, const string &inputStrRaw, const EVP_MD *md)
{
    EVP_MD_CTX* ctx = EVP_MD_CTX_create();
    if ( ! ctx ) return false;
    if ( EVP_DigestInit_ex(ctx, md, NULL) != 1 ) 
        return false;
    if ( EVP_DigestUpdate(ctx, inputStrRaw.c_str(), inputStrRaw.length()) != 1)
        return false;
    
    unsigned char hash[EVP_MAX_MD_SIZE];
    int length;
    if ( EVP_DigestFinal_ex(ctx, hash,(unsigned int*) &length) != 1 )
        return false;

    outputStrRaw = string(reinterpret_cast<char*>(hash), length);
    EVP_MD_CTX_destroy(ctx);
    return true;
}

char bitsToHexChar(unsigned int bits) {
    if ( bits < 10 ) {
        return '0' + bits;
    }
    else if ( bits <= 0xf) {
        return 'a' + bits - 10;
    }
    else {
        assert(false);
    }
}

string strRawToHex(const string& strRaw) {
    string strHex;
    for ( unsigned char byte : strRaw ) {
        char left = bitsToHexChar((byte >> 4) & 0x0f);
        char right = bitsToHexChar(byte & 0x0f);
        strHex.push_back(left);
        strHex.push_back(right);
    }
    return strHex;
}

void incrementStrRaw(string& strRaw) {
    bool allZero = false;
    for ( size_t i = 0; i < strRaw.size(); i ++ ) {
        strRaw[i] ++;
        // if overflow didn't occur
        if ( strRaw[i] != 0 ) {
            break;
        } 
        if ( i + 1 == strRaw.size() ) {
            allZero = true;
        }
    }
    if ( allZero ) {
        strRaw.push_back(0);
    }
}


int findHashEx(int numberZeroBits, string &outputMessage, string &outputHash, string_view hashType);
int findHash(int numberZeroBits, string &outputMessage, string &outputHash)
{
    /* TODO: Your code here */
    return findHashEx(numberZeroBits, outputMessage, outputHash, "sha512");
}

int findHashEx(int numberZeroBits, string &outputMessage, string &outputHash, string_view hashType)
{
    /* TODO or use dummy implementation */
    OpenSSL_add_all_digests();
    const EVP_MD *md = EVP_get_digestbyname(hashType.data());
    if (!md || numberZeroBits < 0 || numberZeroBits > EVP_MD_size(md) * 8)
    {
        return 0;
    }
    string strRaw;
    strRaw.push_back(0);
    while ( true ) {
        string hashRaw;
        if ( ! computeHash(hashRaw, strRaw, md) ) {
            return 0;
        }
        if ( cntZerosInStrRaw(hashRaw) == numberZeroBits ) {
            outputHash = strRawToHex(hashRaw);
            outputMessage = strRawToHex(strRaw);
            break;
        }
        incrementStrRaw(strRaw);
    }

    return 1;
}

#ifndef __PROGTEST__

int checkHash(int bits, const string &hash)
{
    // DIY
    int cnt = 0;
    for ( char x : hash ) {
        if ( x == '0' ) {
            cnt += 4;
            continue;
        } 
        else if ( x == '1' ) {
            cnt += 3;
        }
        else if ( x == '2' || x == '3' ) {
            cnt += 2;
        }
        else if ( x >= '4' && x <= '7' ) {
            cnt += 1;
        } 
        else if (!(x>='0' && x<='9') && !(x>='a' && x<='f') ){
            cout << x << endl;
            assert (false);
        }

        break;
    }
    return cnt == bits;
}

int main(void)
{
    string hash, message;
    assert(findHash(0, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(0, hash));
    cout << "hash: " << hash << endl;
    cout << "message: " << message << endl << endl;
    message.clear();
    hash.clear();

    assert(findHash(1, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(1, hash));
    message.clear();
    hash.clear();

    assert(findHash(2, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(2, hash));
    message.clear();
    hash.clear();

    assert(findHash(3, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(3, hash));
    cout << "hash: " << hash << endl;
    cout << "message: " << message << endl << endl;
    message.clear();
    hash.clear();

    assert(findHash(-1, message, hash) == 0);

    cout << "**************** self test **************************" << endl;
    for ( int i = 20; i <= 22; i ++ ) {
        cout << "i: " << i << endl;
        assert(findHash(i, message, hash) == 1);
        assert(!message.empty() && !hash.empty() && checkHash(i, hash));
        cout << "hash: " << hash << endl;
        cout << "message: " << message << endl << endl;
        message.clear();
        hash.clear();
    }
    return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */
